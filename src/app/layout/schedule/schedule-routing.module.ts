import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from './schedule.component';
import { ShowtimesComponent } from './showtimes/showtimes.component';
import { BookingComponent } from './booking/booking.component';

const routes: Routes = [
  {
    path: '',
    component: ScheduleComponent,
    children: [
      { path: '', redirectTo: 'showtimes', pathMatch: 'full' },
      {
        path: 'showtimes',
        component: ShowtimesComponent,
      },
      {
        path: 'booking/:id',
        component: BookingComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleRoutingModule { }
