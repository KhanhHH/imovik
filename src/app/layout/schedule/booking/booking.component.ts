import { BalanceService } from './../../../services/balance.service';
import { LoginService } from './../../../services/login.service';
import { AccountService } from './../../../services/account.service';
import { PaymentService } from './../../../services/payment.service';
import { BookingService } from './../../../services/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { from, Subscription } from 'rxjs';
import { map, filter, toArray, mergeMap, mergeAll, distinct } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, OnDestroy {

  filmId = '';
  timeId = '';

  refresh: any;
  bookingSubscription: Subscription;

  totalSelectedSeat = 0;
  ticketPrice = 90000;
  totalPrice = 0;

  serverMessage = {
    message: '',
    type: ''
  };

  modalRef: BsModalRef;
  @ViewChild('message') messageEl: ElementRef;

  seats: any = [
    {
      row: 'a',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'b',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'd',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'e',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'f',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'g',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    },
    {
      row: 'h',
      col: [
        {
          seat_id: 1,
          available: true
        },
        {
          seat_id: 2,
          available: true
        },
        {
          seat_id: 3,
          available: true
        },
        {
          seat_id: 4,
          available: true
        },
        {
          seat_id: 5,
          available: true
        },
        {
          seat_id: 6,
          available: true
        },
        {
          seat_id: 7,
          available: true
        },
        {
          seat_id: 8,
          available: true
        },
        {
          seat_id: 9,
          available: true
        },
        {
          seat_id: 10,
          available: true
        },
        {
          seat_id: 11,
          available: true
        },
        {
          seat_id: 12,
          available: true
        },
        {
          seat_id: 13,
          available: true
        }
      ]
    }
  ];


  selectedSeats: any = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: BsModalService,
    private accountService: AccountService,
    private bookingService: BookingService,
    private paymentService: PaymentService,
    private loginService: LoginService,
    private balanceService: BalanceService
  ) {

  }

  ngOnInit() {
    this.filmId = this.route.parent.snapshot.paramMap.get('id');
    this.timeId = this.route.snapshot.paramMap.get('id');
    this.updateSeats();
    this.refresh = setInterval(() => { this.updateSeats(); }, 5000);
  }

  updateSeats() {
    this.bookingSubscription = this.bookingService.getSeats(this.timeId).pipe(
      mergeAll(),
      mergeMap((res: any) => {
        this.seats.map((s: any) => {
          if (s.row === res.row) {
            s.col.map((c: any) => {
              res.col.map((resC: any) => {
                if (c.seat_id === resC.seat_id) {
                  c.available = resC.available;
                }
              });
            });
          }
        });
        return res.col;
      }),
      toArray()
    ).subscribe((data: any) => {
      console.log('Refreshing data');
    });
  }

  countSelectedSeat() {
    from(this.seats).pipe(
      mergeMap((row: any) => {
        return row.col.map((seat: any) => {
          return {
            row: row.row,
            seat_id: seat.seat_id,
            available: seat.available,
            selected: seat.selected
          };
        });
      }),
      filter((data: any) => data.selected === true),
      toArray()
    ).subscribe(data => {
      this.selectedSeats = data;
      this.totalSelectedSeat = data.length;
      this.totalPrice = this.ticketPrice * this.totalSelectedSeat;
      console.log('Selected seats:', this.selectedSeats);
    });
  }

  pay() {

    if (this.accountService.isTokenExpired() === false) {
      const paymentSeats = this.selectedSeats.map((data: any) => {
        return {
          row: data.row,
          seat_id: data.seat_id
        };
      });
      const paymentInfo = {
        user_id: this.accountService.getAccountInfo().id,
        time_id: this.timeId,
        film_id: this.filmId,
        price: this.totalPrice,
        seats: paymentSeats
      };

      console.log('Payment info:', paymentInfo);

      this.paymentService.postPaymentBooking(paymentInfo).subscribe((data: any) => {
        console.log(data);
        if (data.success === true) {
          this.balanceService.getAccountBalance().subscribe((bl: any) => {
            this.serverMessage = {
              message: data.message,
              type: 'success'
            };
            this.accountService.balance$.next(bl.result.balance);
          });
          this.modalRef = this.modalService.show(this.messageEl);
          this.modalService.onHidden.subscribe((reason: any) => {
            const currentRoute = this.router.url.split('/')[3];
            console.log(currentRoute);
            if (currentRoute === 'booking'){
              this.router.navigateByUrl('/account/booking-history/detail/' + data.result.id);
            }
          });
        } else {
          this.serverMessage = {
            message: data.message,
            type: 'failure'
          }
          this.modalRef = this.modalService.show(this.messageEl);
        }
      }, (err: any) => {
        console.log(err);
      });
    } else {
      console.log('logout');
      this.loginService.isLoggedIn$.next(false);
    }

  }

  ngOnDestroy(): void {
    clearInterval(this.refresh);
    this.bookingSubscription.unsubscribe();
  }

}
