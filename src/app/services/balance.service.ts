import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;
  constructor(
    private http: HttpClient
  ) { }

  getAccountBalance() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };

    return this.http.get(this.BASE_API_URL + '/account/balance', httpOptions);
  }
}
