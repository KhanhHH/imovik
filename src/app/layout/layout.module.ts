import { ComponentsModule } from './../components/components.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    ModalModule.forRoot(),
    ComponentsModule,
    CommonModule,
    LayoutRoutingModule,
    FormsModule
  ]
})
export class LayoutModule { }
