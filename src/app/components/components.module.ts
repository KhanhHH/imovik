import { FormsModule } from '@angular/forms';
import { MessageComponent } from './message/message.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [LoginComponent, MessageComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    LoginComponent, MessageComponent
  ]
})
export class ComponentsModule { }
