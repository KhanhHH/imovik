import { BookingHistoryService } from './../../../../services/booking-history.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking-history-list',
  templateUrl: './booking-history-list.component.html',
  styleUrls: ['./booking-history-list.component.scss']
})
export class BookingHistoryListComponent implements OnInit {

  bookingHistoryList = [];

  constructor(
    private bookingHistoryService: BookingHistoryService
  ) { }

  ngOnInit() {
    this.bookingHistoryService.getBookingHistoryList().subscribe((data: any) => {
      this.bookingHistoryList = data;
      console.log(this.bookingHistoryList);
    })
  }

}
