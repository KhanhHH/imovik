import { CinemaService } from './../../../services/cinema.service';
import { ScheduleService } from './../../../services/schedule.service';
import { MovieService } from './../../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { mergeAll, distinct, toArray, map, mergeMap, groupBy } from 'rxjs/operators';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-showtimes',
  templateUrl: './showtimes.component.html',
  styleUrls: ['./showtimes.component.scss']
})
export class ShowtimesComponent implements OnInit {

  cinemas = [
    // {
    //   cinema_group: 'bhd',
    //   name: 'BHD Star Cineplex'
    // }
    // ,
    // {
    //   cinema_group: 'cgv',
    //   name: 'CGV Cinema'
    // }
    // ,

  ]

  dates = [];

  showtimes = [];

  selectedSchedule: any = {
    film_id: '',
    cinema_group: '',
    date: '',
  };


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private movieService: MovieService,
    private scheduleService: ScheduleService,
    private cinemaService: CinemaService
  ) { }

  ngOnInit() {
    // this.selectedSchedule.film_id = this.route.parent.snapshot.paramMap.get('id');
    // this.selectedSchedule.cinema_group = 'bhd';
    // this.selectedSchedule.date = new Date(Date.now()).toLocaleString().split(',')[0];
    this.selectedSchedule = {
      film_id: this.route.parent.snapshot.paramMap.get('id'),
      cinema_group: 'bhd',
      date: formatDate(new Date(), 'MM/dd/yyyy', 'en-US')
    };
    this.cinemaService.getCinemaList().pipe(
      mergeAll(),
      map((data: any) => {
        return {
          cinema_group: data.group,
          name: data.name
        };
      }),
      toArray()
    )
      .subscribe((res: any) => {
        this.cinemas = res;
        console.log(res);
      });
    this.scheduleService.postScheduleShowtimesDate({
      film_id: this.selectedSchedule.film_id
    }).pipe(
      mergeAll(),
      distinct((data: any) => data.date),
      map((data: any) => data.date),
      toArray()
    )
      .subscribe((res: any) => {
        res.sort();
        this.dates = res.map((data: any) => formatDate(data, 'MM/dd/yyyy', 'en-US'));
        console.log(res);
      }, (err) => {
        console.log(err);
      });
    this.updateSchedule();
  }

  updateSchedule() {
    this.scheduleService.postScheduleShowtimes(this.selectedSchedule).subscribe((res: any) => {
      this.showtimes = res;
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  selectCinema(cinema_group: any) {
    this.selectedSchedule.cinema_group = cinema_group;
    this.updateSchedule();
  }

  selectDate(date: any) {
    this.selectedSchedule.date = date;
    this.updateSchedule();
  }

}
