import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './../services/login.service';
import { AccountService } from './../services/account.service';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  @ViewChild('login') loginEl: ElementRef;

  modalRef: BsModalRef;

  selected = false;

  isLoggedIn = false;

  accountInfo = {
    id: '',
    email: '',
    display_name: '',
    // balance: 0
  };

  balance = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private accountService: AccountService,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.loginService.isLoggedIn$.subscribe((status) => {
      const currentRoute = this.router.url.split('/')[3];

      console.log(currentRoute);
      this.isLoggedIn = status;
      console.log(this.isLoggedIn);
      if (this.isLoggedIn === true) {
        this.accountInfo = this.accountService.getAccountInfo();
      }
      if (this.isLoggedIn === false) {
        if (currentRoute === 'booking') {
          this.modalService.show(this.loginEl);
        }

      }
    })
    if (this.accountService.isTokenExpired() === false) {
      this.loginService.isLoggedIn$.next(true);
      this.accountInfo = this.accountService.getAccountInfo();
    }

    this.accountService.balance$.subscribe((data) => {
      this.balance = data;
    });
  }


  openModal() {
    this.modalRef = this.modalService.show(this.loginEl);
  }

  logout() {
    localStorage.removeItem('access_token');
    this.accountService.removeAccountInfo();
    this.loginService.isLoggedIn$.next(false);
    this.router.navigateByUrl('/home');
  }
}
