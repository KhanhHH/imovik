import { BsModalRef } from 'ngx-bootstrap/modal';
import { Injectable, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from './global';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  isLoggedIn$ = new BehaviorSubject(false);

  constructor(
    private http: HttpClient
  ) { }

  postLogin(loginForm) {
    const body = {
      email: loginForm.email,
      password: loginForm.password
    };
    console.log('req', body)
    return this.http.post(this.BASE_API_URL + '/account/login', body);
  }
  postRegister(registerForm) {
    const body = {
      display_name: registerForm.display_name,
      email: registerForm.email,
      password: registerForm.password,
      verify_password: registerForm.verify_password
    };
    console.log('req', body)
    return this.http.post(this.BASE_API_URL + '/account/create', body);
  }
}
