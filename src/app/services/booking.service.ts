import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getSeats(timeId: any) {
    return this.http.get(this.BASE_API_URL + '/booking/seats/' + timeId);
  }
}
