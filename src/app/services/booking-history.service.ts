import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from './global';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class BookingHistoryService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;
  constructor(
    private http: HttpClient
  ) { }

  getBookingHistoryList() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get(this.BASE_API_URL + '/account/booking-history/list', httpOptions);
  }
  getBookingHistoryDetail(id: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get(this.BASE_API_URL + '/account/booking-history/detail/' + id, httpOptions);
  }
}

