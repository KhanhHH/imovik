import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { BookingHistoryDetailComponent } from './booking-history/booking-history-detail/booking-history-detail.component';
import { BookingHistoryListComponent } from './booking-history/booking-history-list/booking-history-list.component';
import { TopUpComponent } from './top-up/top-up.component';

@NgModule({
  declarations: [AccountComponent, BookingHistoryComponent, BookingHistoryDetailComponent, BookingHistoryListComponent, TopUpComponent],
  imports: [
    CommonModule,
    FormsModule,
    AccountRoutingModule
  ]
})
export class AccountModule { }
