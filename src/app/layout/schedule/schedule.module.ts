import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowtimesComponent } from './showtimes/showtimes.component';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { ScheduleComponent } from './schedule.component';
import { BookingComponent } from './booking/booking.component';
import { ComponentsModule } from 'src/app/components/components.module';





@NgModule({
  declarations: [ShowtimesComponent, ScheduleComponent, BookingComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ScheduleRoutingModule
  ]
})
export class ScheduleModule { }
