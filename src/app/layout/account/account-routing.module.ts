import { BookingHistoryListComponent } from './booking-history/booking-history-list/booking-history-list.component';
import { BookingHistoryDetailComponent } from './booking-history/booking-history-detail/booking-history-detail.component';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { AccountComponent } from './account.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopUpComponent } from './top-up/top-up.component';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    children: [
      { path: '', redirectTo: 'booking-history', pathMatch: 'full' },
      {
        path: 'booking-history',
        component: BookingHistoryComponent,
        children: [
          {
            path: '',
            component: BookingHistoryListComponent,
          },
          {
            path: 'detail/:id',
            component: BookingHistoryDetailComponent,
          }
        ]
      },
      {
        path: 'top-up',
        component: TopUpComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
