import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getBannerList() {
    return this.http.get(this.BASE_API_URL + '/home/banner');
  }
}
