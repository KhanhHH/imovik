import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getMovieList() {
    return this.http.get(this.BASE_API_URL + '/movie/list');
  }

  getMovieDetail(id) {
    return this.http.get(this.BASE_API_URL + '/movie/detail/' + id);
  }
}
