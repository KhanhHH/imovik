import { BalanceService } from './../../../services/balance.service';
import { PaymentService } from './../../../services/payment.service';
import { AccountService } from './../../../services/account.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-up',
  templateUrl: './top-up.component.html',
  styleUrls: ['./top-up.component.scss']
})
export class TopUpComponent implements OnInit {

  balance = 0;
  topupAmount = 0;

  topupSuccess = false;

  constructor(
    private accountService: AccountService,
    private paymentService: PaymentService,
    private balanceService: BalanceService
  ) { }

  ngOnInit() {
    this.balanceService.getAccountBalance().subscribe((data: any) => {
      this.balance = data.result.balance;
    });
  }

  topup() {
    const topupAmountRequest = {
      amount: this.topupAmount
    };
    this.paymentService.postPaymentTopUp(topupAmountRequest).subscribe((data: any) => {
      console.log(data);
      this.balance = data.result;
      this.accountService.balance$.next(data.result);
      this.topupSuccess = true;
    })
  }

}
