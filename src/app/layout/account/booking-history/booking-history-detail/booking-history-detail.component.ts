import { BookingHistoryService } from './../../../../services/booking-history.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking-history-detail',
  templateUrl: './booking-history-detail.component.html',
  styleUrls: ['./booking-history-detail.component.scss']
})
export class BookingHistoryDetailComponent implements OnInit {

  isExpired = false;
  warnMessage = '';

  bookingHistoryDetail = {
    movie_title: '',
    poster_img: '',
    purchase_date: '',
    premiering_date: '',
    premiering_time: '',
    price: '',
    seats: '',
    ticket_code: ''
  };

  constructor(
    private route: ActivatedRoute,
    private bookingHistoryService: BookingHistoryService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.bookingHistoryService.getBookingHistoryDetail(id).subscribe((data: any) => {
      console.log(data);
      if (data.success === true) {
        this.isExpired = false;
        const convertedSeatsName = data.result.seats.map((seat: any) => {
          const flatName = seat.row + '-' + seat.seat_id;
          return flatName.toUpperCase();
        });
        this.bookingHistoryDetail = {
          movie_title: data.result.title,
          poster_img: data.result.poster_img,
          purchase_date: data.result.purchase_date,
          premiering_date: data.result.date,
          premiering_time: data.result.time,
          price: data.result.price,
          seats: convertedSeatsName.join(', '),
          ticket_code: data.result.ticket_code
        }
      } else {
        this.isExpired = true;
        this.warnMessage = data.message;
      }


    });
  }

}
