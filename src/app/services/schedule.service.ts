import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from './global';


@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  postScheduleShowtimes(req: any) {
    return this.http.post(this.BASE_API_URL + '/schedule/list', req);
  }

  postScheduleShowtimesDate(req: any) {
    return this.http.post(this.BASE_API_URL + '/schedule/list/date', req);
  }

}
