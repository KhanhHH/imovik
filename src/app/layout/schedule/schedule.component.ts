import { MovieService } from './../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {


  intro: any = {
    title: '',
    description: '',
    classify: '',
    director: '',
    cast: '',
    genre: '',
    release: '',
    running: '',
    language: '',
    poster_img: ''
  };

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.movieService.getMovieDetail(id).subscribe((res: any) => {
      this.intro = res;
    });
  }

}
