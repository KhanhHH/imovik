import { HomeService } from './../../services/home.service';
import { MovieService } from './../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, noPause: true, showIndicators: true } }
  ]
})
export class HomeComponent implements OnInit {

  banners: any = [];

  movies: any = {
    premiering: [],
    upcoming: []
  };

  constructor(
    private homeService: HomeService,
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.homeService.getBannerList().subscribe((res: any) => {
      this.banners = res;
    });
    this.movieService.getMovieList().subscribe((res: any) => {
      res.map(data => {
        if (data.status === 'premiering') {
          this.movies.premiering.push({
            film_id: data.film_id,
            img: data.poster_img
          });
        }
        if (data.status === 'upcoming') {
          this.movies.upcoming.push({
            film_id: data.film_id,
            img: data.poster_img
          });
        }
      });
    }, (err: any) => {
      console.log(err);
    });
    console.log(this.movies);
  }

  scrollLeft(element) {
    document.getElementById(element).scrollLeft -= 600;
  }
  scrollRight(element) {
    document.getElementById(element).scrollLeft += 600;
  }

}
