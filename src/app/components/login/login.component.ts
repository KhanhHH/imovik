import { BehaviorSubject } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoginService } from './../../services/login.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  route = 'login';

  loginSuccess = false;

  loginMessage = '';
  registerMessage = '';

  loginForm = {
    email: '',
    password: ''
  };

  registerForm = {
    display_name: '',
    email: '',
    password: '',
    verify_password: ''
  };

  constructor(
    private modalService: BsModalService,
    private loginService: LoginService,

  ) { }

  ngOnInit() {
    this.loginService.isLoggedIn$.subscribe((status) => {
      if (status === true) {
        this.loginSuccess = status;
      }
    })
  }

  login() {
    this.loginService.postLogin(this.loginForm).subscribe((res: any) => {
      console.log(res);
      this.loginMessage = res.message;
      if (res.success === true) {
        localStorage.setItem('access_token', res.token);
        this.loginService.isLoggedIn$.next(true);
      }
    }, (err) => {
      console.log(err);
    });
  }

  register() {
    this.loginService.postRegister(this.registerForm).subscribe((res: any) => {
      console.log(res);
      this.registerMessage = res.message;
      if (res.success === true) {

      }
    }, (err) => {
      console.log(err);
    });

  }

  onClickRegister() {
    this.route = 'register';
  }

  onClickBack() {
    this.route = 'login';
  }

}
