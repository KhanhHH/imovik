import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getCinemaList() {
    return this.http.get(this.BASE_API_URL + '/cinema/list');
  }

}
