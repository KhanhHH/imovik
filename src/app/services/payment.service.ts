import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from './global';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;
  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     // tslint:disable-next-line: object-literal-key-quotes
  //     'Authorization': 'Bearer ' + localStorage.getItem('access_token')
  //   })
  // };
  constructor(
    private http: HttpClient
  ) { }

  postPaymentBooking(req: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };
    return this.http.post(this.BASE_API_URL + '/payment/booking', req, httpOptions);
  }
  postPaymentTopUp(req: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      })
    };
    console.log(req);
    return this.http.post(this.BASE_API_URL + '/payment/top-up', req, httpOptions);
  }
}
