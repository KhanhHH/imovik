import { BehaviorSubject, Subject } from 'rxjs';
import { BalanceService } from './balance.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from './global';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  BASE_API_URL = GlobalVariable.BASE_API_URL;

  accountInfo = {
    id: '',
    email: '',
    display_name: '',
  };

  balance$ = new BehaviorSubject(0);

  helper = new JwtHelperService();

  constructor(
    private http: HttpClient,
    private balanceService: BalanceService
  ) { }

  getAccountInfo() {
    this.getTokenInfo();
    return this.accountInfo;
  }

  getTokenInfo() {
    const localToken = localStorage.getItem('access_token');
    if (localToken) {
      const decodedToken = this.helper.decodeToken(localToken);
      this.accountInfo = {
        id: decodedToken.id,
        email: decodedToken.email,
        display_name: decodedToken.display_name,
      };
      this.balanceService.getAccountBalance().subscribe((data: any) => {
        this.balance$.next(data.result.balance);
        console.log('balance', data);
      });
    }
  }

  isTokenExpired() {
    const localToken = localStorage.getItem('access_token');
    return this.helper.isTokenExpired(localToken);
  }

  removeAccountInfo() {
    this.accountInfo = {
      id: '',
      email: '',
      display_name: '',
    };
    this.balance$.next(0);
  }



}
